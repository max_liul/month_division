import pandas as pd
import numpy as np


def activity_periods_month(path_to_data_frame, date_column,
                            transaction_column, # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                            column_for_analysis,
                            number_of_months_in_interval,
                            is_normalized = None,
                            output_file = None,
                            product_column='StockCode',
                            customer_column='CustomerID',
                            quantity_column='Quantity',
                            list_of_excluded_months = None,
                            not_consider_first_and_last_months = False):
                                                                    # excluded_months is a list of month (from 1 to 12) which are excluded from the consideration

    if not_consider_first_and_last_months:
        df_temp = pd.read_csv(path_to_data_frame)
    # convert data string to month (int)
        df_temp[date_column] = pd.to_datetime(df_temp[date_column])
        max_month = df_temp[date_column].max().month
        min_month = df_temp[date_column].min().month

        start_index_of_df = df_temp.loc[df_temp[date_column].dt.month == min_month+1].first_valid_index()
        finish_index_of_df = df_temp.loc[df_temp[date_column].dt.month  == max_month - 1].last_valid_index()

        df = df_temp.loc[(df_temp.index >= start_index_of_df) & (df_temp.index < finish_index_of_df)].copy()
    else:
        df = pd.read_csv(path_to_data_frame)
        df[date_column] = pd.to_datetime(df[date_column])

    if transaction_column == 'index':
        transaction_column = 'transaction_id'
        df[transaction_column] = df.index
    if list_of_excluded_months is None:
        list_of_excluded_months = []

    if number_of_months_in_interval == 1:

        df[date_column] = df[date_column].dt.month_name()
        # exclusion of hours, chosen in list_of_excluded_hours
        df_selected = df.loc[~(df[date_column].isin(list_of_excluded_months))].copy()
        df_selected['Time Interval'] = df_selected[date_column]

        # create an array of considered months
        sorted_columns = df_selected[date_column].unique()

    else:
        df[date_column] = df[date_column].dt.month
        # exclusion of hours, chosen in list_of_excluded_hours
        df_selected = df.loc[~(df[date_column].isin(list_of_excluded_months))].copy()

        # create an array of considered months
        list_of_considered_months = df_selected[date_column].unique()
        list_of_considered_months.sort()
        # append an extra element to the end of array in order not to have problems with names of columns
        list_of_considered_months = np.append(list_of_considered_months,
                                              list_of_considered_months[len(list_of_considered_months) - 1] + 1)

        # creation of 'Time Interval' column and full filling it by the strings what correspond to the interval what is suitable for a considered product_or_user
        min_months_value_in_interval = 0
        while min_months_value_in_interval < len(list_of_considered_months) - 1:
            # we use min() function in order to prevent the situation when index will exceed an array's length
            df_selected.loc[(df_selected[date_column] >= list_of_considered_months[
                min_months_value_in_interval]) & (df_selected[date_column] <= list_of_considered_months[
                min(min_months_value_in_interval + number_of_months_in_interval - 1,
                    len(list_of_considered_months) - 1)]), 'Time Interval'] = \
                '{}-{}'.format(list_of_considered_months[min_months_value_in_interval],
                               list_of_considered_months[
                                   min(min_months_value_in_interval + number_of_months_in_interval,
                                       len(list_of_considered_months) - 1)])

            min_months_value_in_interval += number_of_months_in_interval
        sorted_columns = df_selected['Time Interval'].unique()

    # creation of a df, where indexes are product_or_user and colums are time intervals
    if column_for_analysis == product_column:
        df_grouped_by_column_for_analysis_time_interval = df_selected.groupby([column_for_analysis, 'Time Interval'])[
                quantity_column].sum().unstack().fillna(0)
    elif column_for_analysis == customer_column:
            # here  [quantity_column].count() is just a dummy; without it we can not use a grouped df on the next step.
            # on my home computer [transaction_column].count() made an error "cannot insert Transaction ID, already exists"
        df_grouped_by_column_for_analysis_time_interval_transaction = \
        df_selected.groupby([column_for_analysis, transaction_column, 'Time Interval']
                                , as_index=False)[quantity_column].count()
        # counting the number of visits for each user in a considered time interval
        df_grouped_by_column_for_analysis_time_interval = \
        df_grouped_by_column_for_analysis_time_interval_transaction.groupby(
                [column_for_analysis, 'Time Interval'])[transaction_column].count().unstack().fillna(0)
    else:
        return print('column_for_analysis is not correct')

    df_grouped_by_column_for_analysis_time_interval = df_grouped_by_column_for_analysis_time_interval[
            sorted_columns]
    df_grouped_by_column_for_analysis_time_interval['Total'] = df_grouped_by_column_for_analysis_time_interval.sum(
            axis=1)

    # normalization: create a list of intervals (columns of a df_grouped_by_column_for_analysis_time_interval)
    # and iterate over them, dividing on a total amount of visits/quantities

    if is_normalized is not None:
        for interval in sorted_columns:
            df_grouped_by_column_for_analysis_time_interval[interval] = \
                    df_grouped_by_column_for_analysis_time_interval[interval] / \
                    df_grouped_by_column_for_analysis_time_interval['Total']
    if output_file is not None:

        return df_grouped_by_column_for_analysis_time_interval.sort_values('Total', ascending=False).reset_index().to_csv(
                            '{}.csv'.format(output_file), index = False)
    else:
        return df_grouped_by_column_for_analysis_time_interval.sort_values('Total', ascending=False).reset_index()

activity_periods_month('kauia_dataset_excluded_extras.csv',
                            'Transaction Date',
                            'Transaction ID', # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                            'Product Name',
                            number_of_months_in_interval = 3,
                            is_normalized = True,
                            output_file = 'Test',
                            product_column='Product Name',
                            customer_column='Member ID',
                            quantity_column='Product Quantity',
                            list_of_excluded_months = [],
                            not_consider_first_and_last_months = False)


