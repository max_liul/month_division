import pandas as pd
import numpy as np


def activity_named_month(path_to_data_frame, date_column,
                            transaction_column, # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                            column_for_analysis,
                            is_normalized = None,
                            output_file = None,
                            product_column='StockCode',
                            customer_column='CustomerID',
                            quantity_column='Quantity',
                            list_of_excluded_months = []):
                                                                    # excluded_months is a list of month (from 1 to 12) which are excluded from the consideration

    df = pd.read_csv(path_to_data_frame)
    # convert data string to month (int)
    df[date_column] = pd.to_datetime(df[date_column])
    df[date_column] = df[date_column].dt.month_name()

    # exclusion of hours, chosen in list_of_excluded_hours
    df_selected = df.loc[~(df[date_column].isin(list_of_excluded_months))].copy()

    # create an array of considered months
    list_of_considered_months = df_selected[date_column].unique()

   # creation of a df, where indexes are product_or_user and colums are time intervals
    if column_for_analysis == product_column:
        df_grouped_by_column_for_analysis_time_interval = df_selected.groupby([column_for_analysis, date_column])[quantity_column].sum().unstack().fillna(0)
    elif column_for_analysis == customer_column:
        # here  [quantity_column].count() is just a dummy; without it we can not use a grouped df on the next step.
        # on my home computer [transaction_column].count() made an error "cannot insert Transaction ID, already exists"
        df_grouped_by_column_for_analysis_time_interval_transaction = df_selected.groupby([column_for_analysis, transaction_column, date_column]
                                                                                          ,as_index=False)[quantity_column].count()
        #counting the number of visits for each user in a considered time interval
        df_grouped_by_column_for_analysis_time_interval = df_grouped_by_column_for_analysis_time_interval_transaction.groupby(
                                                                [column_for_analysis, date_column])[transaction_column].count().unstack().fillna(0)
    else:
        print('column_for_analysis is not correct')

    df_grouped_by_column_for_analysis_time_interval = df_grouped_by_column_for_analysis_time_interval[list_of_considered_months]
    df_grouped_by_column_for_analysis_time_interval['Total'] = df_grouped_by_column_for_analysis_time_interval.sum(axis=1)

    # normalization: create a list of intervals (columns of a df_grouped_by_column_for_analysis_time_interval)


     # and iterate over them, dividing on a total amount of visits/quantities

    if is_normalized is not None:
        for interval in list_of_considered_months:
            df_grouped_by_column_for_analysis_time_interval[interval] = \
                df_grouped_by_column_for_analysis_time_interval[interval] / \
                df_grouped_by_column_for_analysis_time_interval['Total']

    if output_file is not None:

        return df_grouped_by_column_for_analysis_time_interval.sort_values('Total', ascending=False).reset_index().to_csv(
                            '{}.csv'.format(output_file), index = False)
    else:
        return df_grouped_by_column_for_analysis_time_interval.sort_values('Total', ascending=False).reset_index()

activity_named_month('kauia_dataset_excluded_extras.csv',
                            'Transaction Date',
                            'Transaction ID', # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                            'Product Name',
                            output_file = 'Test_1',
                            is_normalized=None,
                            product_column='Product Name',
                            customer_column='Member ID',
                            quantity_column='Product Quantity',
                            list_of_excluded_months = [])


